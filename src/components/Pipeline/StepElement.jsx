import { stepType, archiveType } from '@/types.js'
import {
  StepStatus,
  StepStatusColor,
  StepStatusLabel,
  StepNameLabel,
  ActionButtonDescription,
} from '@/utils.js'
import PropTypes from 'prop-types'
import React from 'react'
import { Header, Segment, Icon, Loader, Popup } from 'semantic-ui-react'

export const ActionButton = {
  redo: (onClick, value) => (
    <ToolTipActionButton
      name="redo"
      onClick={onClick}
      value={value}
      disabled={false}
    />
  ),
  play: (onClick, value) => (
    <ToolTipActionButton
      name="play"
      onClick={onClick}
      value={value}
      disabled={false}
    />
  ),
  remove: (onClick, value) => (
    <ToolTipActionButton
      name="cancel"
      onClick={onClick}
      value={value}
      disabled={true}
    />
  ),
}

/**
 * This component renders each one of the steps only if it completed
 * or if it is the last one. If a step is failed and it is the latest one
 * shows a retry button
 */
export class StepElement extends React.Component {
  static propTypes = {
    step: stepType.isRequired,
    archive: archiveType,
    onStepRetry: PropTypes.func,
    onStepContinue: PropTypes.func,
    removable: PropTypes.bool,
    onRemove: PropTypes.func,
    canExecuteStep: PropTypes.bool,
  }

  constructor(props) {
    super(props)
    this.state = {
      loading: false,
    }
  }

  onRetry = () => {
    this.setState({ loading: true })
    this.props.onStepRetry()
    this.setState({ loading: false })
  }

  onContinue = () => {
    this.setState({ loading: true })
    this.props.onStepContinue()
    this.setState({ loading: false })
  }

  render() {
    const { step, archive, removable, onRemove, canExecuteStep } = this.props
    const { loading } = this.state
    let color = StepStatusColor[step.status]
    let actionIcon = undefined
    let onClick = undefined

    if (removable) {
      if (!step.status) {
        actionIcon = 'remove'
        onClick = onRemove
      }
    }

    if (archive && archive.last_step)
      if (step.status == StepStatus.FAILED && archive.last_step.id == step.id) {
        // step is the last failed step
        actionIcon = 'redo'
        onClick = this.onRetry
      } else if (
        step.status == StepStatus.WAITING &&
        archive.last_step.id == step.input_step &&
        archive.last_step.status == StepStatus.FAILED
      ) {
        // step is the first (not run) step after the last failed one
        actionIcon = 'play'
        onClick = this.onContinue
      }

    return (
      <StepSegment color={color}>
        <Header as="h5" textAlign="center">
          {StepNameLabel[step.name]}
          <Header.Subheader>{StepStatusLabel[step.status]}</Header.Subheader>
          {canExecuteStep && actionIcon && (
            <Header.Subheader
              style={{
                width: '100%',
                display: 'flex',
                justifyContent: 'center',
              }}
            >
              <div style={{ marginTop: '5px' }}>
                {loading ? (
                  <Loader active inline />
                ) : (
                  <>{ActionButton[actionIcon](onClick, step.id)}</>
                )}
              </div>
            </Header.Subheader>
          )}
        </Header>
      </StepSegment>
    )
  }
}

/**
 * This component renders the circular segment used to diaplay a Step
 */
export class StepSegment extends React.Component {
  static propTypes = {
    color: PropTypes.string,
    children: PropTypes.node,
  }

  constructor(props) {
    super(props)
  }

  render() {
    const { color, children, ...props } = this.props

    return (
      <Segment
        circular
        style={{
          width: '120px',
          height: '120px',
          maxWidth: '120px',
          maxHeight: '120px',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          textAlign: 'center',
          marginTop: '20px',
          marginBottom: '20px',
        }}
        color={color}
        {...props}
      >
        {children}
      </Segment>
    )
  }
}

/**
 * This component renders a a pipeline action button with tooltip
 */
class ToolTipActionButton extends React.Component {
  static propTypes = {
    name: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
    value: PropTypes.number.isRequired,
    disabled: PropTypes.bool.isRequired,
  }

  render() {
    const { name, onClick, value, disabled, ...props } = this.props

    return (
      <Popup
        trigger={
          <Icon
            link
            circular
            style={{
              margin: 0,
              lineHeight: 'normal',
            }}
            onClick={onClick}
            value={value}
            name={name}
          />
        }
        content={ActionButtonDescription[name]}
        inverted
        position="right center"
        disabled={disabled}
        {...props}
      />
    )
  }
}
