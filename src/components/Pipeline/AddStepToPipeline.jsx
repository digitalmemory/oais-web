import { archiveType } from '@/types.js'
import { ArchiveActionName } from '@/utils.js'
import PropTypes, { number } from 'prop-types'
import React from 'react'
import { NextStepElement } from '@/components/Pipeline/NextStepElement.jsx'
import { AddStepElement } from '@/components/Pipeline/AddStepElement.jsx'
import { CreatePipeline } from '@/components/Pipeline/CreatePipeline.jsx'

export class AddStepToPipeline extends React.Component {
  static propTypes = {
    archive: archiveType.isRequired,
    nextSteps: PropTypes.arrayOf(number).isRequired,
    onAddStep: PropTypes.func.isRequired,
    onAddPipeline: PropTypes.func.isRequired,
  }

  constructor(props) {
    super(props)
    this.state = {
      modalIsOpen: false,
    }
  }

  onClose = () => {
    this.setState({ modalIsOpen: false })
  }

  render() {
    const { nextSteps, onAddStep, onAddPipeline } = this.props

    return (
      <>
        <AddStepElement
          direction="top"
          header="Choose Next Action"
          nextSteps={nextSteps}
          onAddStep={onAddStep}
        >
          <NextStepElement
            key={ArchiveActionName.PIPELINE}
            action={ArchiveActionName.PIPELINE}
            onClick={() => this.setState({ modalIsOpen: true })}
          />
        </AddStepElement>
        <CreatePipeline
          initNextSteps={nextSteps}
          open={this.state.modalIsOpen}
          onCreatePipeline={onAddPipeline}
          onClose={this.onClose}
        />
      </>
    )
  }
}
