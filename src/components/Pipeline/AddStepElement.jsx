import PropTypes, { number } from 'prop-types'
import React from 'react'
import { Header, Popup, Grid, Icon } from 'semantic-ui-react'
import { NextStepElement } from '@/components/Pipeline/NextStepElement.jsx'
import { StepSegment } from '@/components/Pipeline/StepElement.jsx'

export class AddStepElement extends React.Component {
  static propTypes = {
    children: PropTypes.node,
    header: PropTypes.string,
    direction: PropTypes.string,
    nextSteps: PropTypes.arrayOf(number).isRequired,
    onAddStep: PropTypes.func.isRequired,
  }

  constructor(props) {
    super(props)
  }

  render() {
    const { children, header, direction, nextSteps, onAddStep } = this.props
    return (
      <>
        {nextSteps.length > 0 && (
          <React.Fragment>
            <Popup
              position={`${direction} center`}
              wide
              positionFixed
              trigger={
                <StepSegment>
                  <Icon name="plus" />
                </StepSegment>
              }
              flowing
              hoverable
            >
              <Grid centered divided>
                <Grid.Column>
                  <Grid.Row>
                    <Header>{header}</Header>
                  </Grid.Row>
                  {nextSteps.map((step) => (
                    <NextStepElement
                      key={step}
                      action={step}
                      onClick={onAddStep}
                    />
                  ))}
                  {children}
                </Grid.Column>
              </Grid>
            </Popup>
          </React.Fragment>
        )}
      </>
    )
  }
}
