import { api } from '@/api.js'
import { AppContext } from '@/AppContext.js'
import PropTypes, { number } from 'prop-types'
import React from 'react'
import { Button, Modal, Loader, Grid } from 'semantic-ui-react'
import { StepElement } from '@/components/Pipeline/StepElement.jsx'
import { AddStepElement } from '@/components/Pipeline/AddStepElement.jsx'

export class CreatePipeline extends React.Component {
  static propTypes = {
    initNextSteps: PropTypes.arrayOf(number).isRequired,
    open: PropTypes.bool.isRequired,
    onCreatePipeline: PropTypes.func.isRequired,
    onClose: PropTypes.func.isRequired,
  }

  constructor(props) {
    super(props)
    this.state = {
      pipeline: [],
      nextSteps: [],
      stepConstraints: {},
    }
  }

  onAddStep = (event, { value }) => {
    this.setState((prevState) => ({
      pipeline: [...prevState.pipeline, value],
      nextSteps: this.state.stepConstraints[value],
    }))
  }

  onRemoveStep = (event, { value }) => {
    this.state.pipeline.splice(value, 1)
    const pipeline = this.state.pipeline

    const nextSteps =
      pipeline.length > 0
        ? this.state.stepConstraints[pipeline.at(-1)]
        : this.props.initNextSteps

    this.setState({
      pipeline: pipeline,
      nextSteps: nextSteps,
    })
  }

  onClose = () => {
    this.setState({ pipeline: [] })
    this.props.onClose()
  }

  onMount = () => {
    this.setState({ nextSteps: this.props.initNextSteps })
  }

  onCreatePipeline = async () => {
    this.setState({ loading: true })
    await this.props.onCreatePipeline(this.state.pipeline)
    this.setState({ loading: false })
    this.onClose()
  }

  loadStepOrderConstraints = async () => {
    const constraints = await api.getStepOrderConstraints()
    this.setState({ stepConstraints: constraints })
  }

  async componentDidMount() {
    await this.loadStepOrderConstraints()
    this.setState({ nextSteps: this.props.initNextSteps })
  }

  static contextType = AppContext.Context

  render() {
    const { open } = this.props
    const { pipeline, nextSteps, loading } = this.state

    return (
      <Modal onMount={() => this.onMount()} onClose={this.onClose} open={open}>
        <Modal.Header>Create and execute pipeline</Modal.Header>
        <Modal.Content>
          <Grid>
            <Grid.Row style={{ justifyContent: 'flex-start' }}>
              {pipeline.map((type, idx) => (
                <Grid.Column key={idx} style={{ marginRight: '80px' }}>
                  <StepElement
                    key={idx}
                    step={{ id: idx, name: type }}
                    removable={true}
                    onRemove={this.onRemoveStep}
                    canExecuteStep={true}
                  />
                </Grid.Column>
              ))}
              <Grid.Column style={{ marginRight: '80px' }}>
                <AddStepElement
                  direction="right"
                  header={'Choose Next Step'}
                  nextSteps={nextSteps}
                  onAddStep={this.onAddStep}
                />
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Modal.Content>
        <Modal.Actions>
          <Button onClick={this.onClose}>Cancel</Button>
          {loading ? (
            <Loader active inline />
          ) : (
            <Button
              type="submit"
              content="Execute pipeline"
              onClick={this.onCreatePipeline}
              disabled={pipeline.length === 0}
              positive
            />
          )}
        </Modal.Actions>
      </Modal>
    )
  }
}
