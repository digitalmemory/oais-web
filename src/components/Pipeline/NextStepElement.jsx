import { ArchiveActionLabel } from '@/utils.js'
import PropTypes from 'prop-types'
import React from 'react'
import { Button, Grid } from 'semantic-ui-react'
/**
 * Shows a grey segment which contains a possible action to be added.
 */
export class NextStepElement extends React.Component {
  static propTypes = {
    action: PropTypes.number.isRequired,
    onClick: PropTypes.func.isRequired,
  }

  constructor(props) {
    super(props)
  }

  render() {
    const { action, onClick } = this.props
    return (
      <Grid.Row textAlign="center" style={{ marginBottom: '5px' }}>
        <Button onClick={onClick} value={action}>
          {ArchiveActionLabel[action]}
        </Button>
      </Grid.Row>
    )
  }
}
