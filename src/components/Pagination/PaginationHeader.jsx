import PropTypes from 'prop-types'
import React from 'react'
import { PageSizeRadio } from '@/components/Pagination/PageSizeRadio'
import { SearchPagination } from '@/components/Pagination/SearchPagination'
import { Grid } from 'semantic-ui-react'

export class PaginationHeader extends React.Component {
  static propTypes = {
    onPageSearch: PropTypes.func.isRequired,
    onPageSizeChange: PropTypes.func.isRequired,
    sizeSelection: PropTypes.array.isRequired,
    hasResults: PropTypes.bool.isRequired,
    activePage: PropTypes.number.isRequired,
    hitsPerPage: PropTypes.number.isRequired,
    totalNumHits: PropTypes.number,
    hideResultsMessage: PropTypes.bool,
  }

  constructor(props) {
    super(props)
  }

  render() {
    return (
      <>
        {!this.props.hideResultsMessage && (
          <p
            style={{
              visibility: this.props.hasResults ? 'visible' : 'hidden',
              marginTop: '10px',
            }}
          >
            Total number of results: {this.props.totalNumHits}
          </p>
        )}

        <Grid
          columns={2}
          verticalAlign="middle"
          style={{
            marginBottom: '10px',
          }}
        >
          <Grid.Row>
            <Grid.Column>
              <SearchPagination
                hasResults={this.props.hasResults}
                activePage={this.props.activePage}
                onSearch={this.props.onPageSearch}
                totalNumHits={this.props.totalNumHits}
                hitsPerPage={this.props.hitsPerPage}
              />
            </Grid.Column>
            <Grid.Column textAlign="right">
              <PageSizeRadio
                onChange={this.props.onPageSizeChange}
                hasResults={this.props.hasResults}
                hitsPerPage={this.props.hitsPerPage}
                sizeSelection={this.props.sizeSelection}
              />
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </>
    )
  }
}
