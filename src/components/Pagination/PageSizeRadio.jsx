import PropTypes from 'prop-types'
import React from 'react'
import { Button, Dropdown } from 'semantic-ui-react'

export class PageSizeRadio extends React.Component {
  static propTypes = {
    hitsPerPage: PropTypes.number.isRequired,
    onChange: PropTypes.func.isRequired,
    hasResults: PropTypes.bool.isRequired,
    sizeSelection: PropTypes.array.isRequired,
  }

  sizeChange = (event, { value }) => {
    event.preventDefault()
    this.props.onChange(value)
  }

  render() {
    return this.props.hasResults ? (
      <div>
        <span>Results per page: </span>
        {this.props.sizeSelection.length > 3 ? (
          <Dropdown text={`${this.props.hitsPerPage}`} button={true}>
            <Dropdown.Menu>
              {this.props.sizeSelection.map((size, idx) => (
                <Dropdown.Item
                  key={idx}
                  text={`${size}`}
                  onClick={(event) => this.sizeChange(event, { value: size })}
                />
              ))}
            </Dropdown.Menu>
          </Dropdown>
        ) : (
          <Button.Group size="small">
            {this.props.sizeSelection.map((size, idx) => (
              <Button
                key={idx}
                active={size === this.props.hitsPerPage}
                value={size}
                onClick={this.sizeChange}
              >
                {size}
              </Button>
            ))}
          </Button.Group>
        )}
      </div>
    ) : null
  }
}
