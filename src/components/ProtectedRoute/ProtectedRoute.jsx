import { AppContext } from '@/AppContext.js'
import PropTypes from 'prop-types'
import React from 'react'
import { Redirect, Route } from 'react-router'
import { connect } from 'react-redux'

/**
 * When trying to access a protected route while unauthenticated, makes the user
 * authenticate and redirects them to the requested page after the login.
 */
class ProtectedRoute extends React.Component {
  static propTypes = {
    component: PropTypes.elementType,
    render: PropTypes.func,
    children: PropTypes.node,
    error: PropTypes.number,
  }

  static contextType = AppContext.Context

  state = {
    redirectError: false,
  }

  componentDidUpdate(prevProps) {
    if (prevProps.error !== this.props.error) {
      if (this.props.error) {
        this.setState({ redirectError: true })
      } else {
        this.setState({ redirectError: false })
      }
    }
  }

  render() {
    const { isLoggedIn } = this.context
    const { error, component, render, children, ...rest } = this.props

    return (
      <Route
        {...rest}
        render={(routeProps) => {
          if (!isLoggedIn) {
            // Pass the current URL as a query parameter.
            // After the login, the user will be redirected to the current page.
            const { location } = routeProps
            const redirect = new URLSearchParams({
              redirect: location.pathname + location.search,
            })

            return (
              <Redirect
                to={{
                  pathname: '/login',
                  search: '?' + redirect.toString(),
                }}
              />
            )
          }

          if (this.state.redirectError)
            return <Redirect to={`/${this.props.error}`} />

          if (component) {
            const Component = component
            return <Component {...routeProps} />
          }
        }}
      />
    )
  }
}

// Binds the redux states with this component's props
const mapStateToProps = (state) => {
  return {
    error: state.status,
  }
}

export default connect(mapStateToProps)(ProtectedRoute)
