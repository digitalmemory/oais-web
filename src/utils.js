import { AppContext } from '@/AppContext.js'
import moment from 'moment'
import { api } from '@/api.js'
import React from 'react'
import { Icon } from 'semantic-ui-react'

/**
 * General purpose utility functions, used across the application.
 *
 */
export const StepStatus = {
  NOT_RUN: 1,
  IN_PROGRESS: 2,
  FAILED: 3,
  COMPLETED: 4,
  WAITING_APPROVAL: 5,
  REJECTED: 6,
  WAITING: 7,
}

export const StepStatusLabel = {
  [StepStatus.NOT_RUN]: 'Not Run',
  [StepStatus.IN_PROGRESS]: 'In progress',
  [StepStatus.FAILED]: 'Failed',
  [StepStatus.COMPLETED]: 'Completed',
  [StepStatus.WAITING_APPROVAL]: 'Waiting for Approval',
  [StepStatus.REJECTED]: 'Rejected',
  [StepStatus.WAITING]: 'Waiting',
}

export const StepStatusColor = {
  [StepStatus.NOT_RUN]: 'grey',
  [StepStatus.IN_PROGRESS]: 'blue',
  [StepStatus.FAILED]: 'red',
  [StepStatus.COMPLETED]: 'green',
  [StepStatus.WAITING_APPROVAL]: 'yellow',
  [StepStatus.REJECTED]: 'red',
  [StepStatus.WAITING]: 'grey',
}

export const StepName = {
  SIP_UPLOAD: 1,
  HARVEST: 2,
  VALIDATION: 3,
  CHECKSUM: 4,
  INVENIO_RDM: 7,
  ARCHIVE: 5,
  EDIT_MANIFEST: 6,
  ANNOUNCE: 8,
  PUSH_SIP_TO_CTA: 9,
  EXTRACT_TITLE: 10,
  NOTIFY_SOURCE: 11,
}

export const StepNameLabel = {
  [StepName.SIP_UPLOAD]: 'SIP Upload',
  [StepName.HARVEST]: 'Harvest',
  [StepName.VALIDATION]: 'Validate',
  [StepName.CHECKSUM]: 'Checksum',
  [StepName.INVENIO_RDM]: 'Push to Registry',
  [StepName.ARCHIVE]: 'Preserve',
  [StepName.EDIT_MANIFEST]: 'Edit Manifest',
  [StepName.ANNOUNCE]: 'Announce',
  [StepName.PUSH_SIP_TO_CTA]: 'Push to Tape',
  [StepName.EXTRACT_TITLE]: 'Extract title',
  [StepName.NOTIFY_SOURCE]: 'Notify source',
}

export const ArchiveActionName = {
  ...StepName,
  PIPELINE: 0,
  RERUN_FAILED: -1,
  CONTINUE_PIPELINE: -2,
}

export const ArchiveActionLabel = {
  ...StepNameLabel,
  [ArchiveActionName.PIPELINE]: 'Create Pipeline',
  [ArchiveActionName.RERUN_FAILED]: 'Rerun last failed step',
  [ArchiveActionName.CONTINUE_PIPELINE]: 'Continue pipeline after failed step',
}

export const ActionButtonDescription = {
  ['redo']: 'Retry failed step',
  ['play']: 'Continue pipeline from this step',
}
export const StepDescription = {
  [StepName.SIP_UPLOAD]: 'SIP Upload',
  [StepName.HARVEST]: 'Harvest',
  [StepName.VALIDATION]: 'Validate',
  [StepName.CHECKSUM]: 'Checksum',
  [StepName.INVENIO_RDM]: 'Register bag in central registry',
  [StepName.ARCHIVE]: 'Create the Preservation Bag',
  [StepName.EDIT_MANIFEST]: 'Edit Manifest',
  [StepName.ANNOUNCE]: 'Announce',
  [StepName.PUSH_SIP_TO_CTA]: 'Copy Preservation Bag to Long Term Storage',
  [StepName.EXTRACT_TITLE]: 'Extract title from the SIP metadata',
  [StepName.NOTIFY_SOURCE]: 'Notify upstream source about the preservation',
}

export const SourceStatus = {
  READY: 1,
  NEEDS_CONFIG_PRIVATE: 2,
  NEEDS_CONFIG: 3,
  INVALID: 4,
}

export const SourceStatusLabel = {
  [SourceStatus.READY]: 'Source ready',
  [SourceStatus.NEEDS_CONFIG_PRIVATE]:
    'Only public records will be available. Configuration is needed for private records.',
  [SourceStatus.NEEDS_CONFIG]:
    'Source unavailable. Additional configuration is needed.',
  [SourceStatus.INVALID]:
    'Source setup is invalid, please contact the service admins.',
}

export const SourceStatusColor = {
  [SourceStatus.READY]: 'green',
  [SourceStatus.NEEDS_CONFIG_PRIVATE]: 'yellow',
  [SourceStatus.NEEDS_CONFIG]: 'red',
  [SourceStatus.INVALID]: 'black',
}

export const ArchiveState = {
  NONE: 1,
  SIP: 2,
  AIP: 3,
}

export const ArchiveStateLabel = {
  [ArchiveState.NONE]: 'None',
  [ArchiveState.AIP]: 'AIP',
  [ArchiveState.SIP]: 'SIP',
}

export const ArchiveStateDescription = {
  [ArchiveState.NONE]: 'No package has been created',
  [ArchiveState.AIP]: 'Archival Information Package',
  [ArchiveState.SIP]: 'Submission Information Package',
}

export const ArchiveStateOrder = {
  [ArchiveState.NONE]: 3,
  [ArchiveState.AIP]: 2,
  [ArchiveState.SIP]: 1,
}

export const Permissions = {
  CAN_APPROVE_ARCHIVE: 'oais.can_approve_all',
  CAN_ACCESS_ARCHIVE_ALL: 'oais.view_archive_all',
  CAN_EXECUTE_STEP: 'oais.can_execute_step',
}

export function hasPermission(user, permission) {
  return user?.permissions.includes(permission) ?? false
}

export function sendNotification(title, body, type, duration = 500) {
  const notification = { title, body, type }
  AppContext.addNotification(notification)
  setTimeout(() => AppContext.clearNotifications(), duration)
}

export function formatDateTime(date) {
  return moment(date).format('YYYY/MM/DD HH:mm:ss')
}

export function getCookie(name) {
  let cookieValue = null
  if (document.cookie && document.cookie !== '') {
    const cookies = document.cookie.split(';')
    for (let i = 0; i < cookies.length; i++) {
      const cookie = cookies[i].trim()
      if (cookie.substring(0, name.length + 1) === name + '=') {
        cookieValue = decodeURIComponent(cookie.substring(name.length + 1))
        break
      }
    }
  }
  return cookieValue
}

export async function getNavbarLabels() {
  /*
  Fetch the staged archives for the user so we can (optionally)
  show the "Staged Archive" menu entry with the number

  This is wrapped in a try catch block because we may call this
  when we still don't know if we're authenticated or not
  (e.g. in the App wrapper before logging in)
  */
  try {
    const StagedArchivesList = await api.stagedArchives()
    if (StagedArchivesList) {
      AppContext.setStaged(StagedArchivesList.length)
    } else {
      AppContext.setStaged(0)
    }
  } catch (err) {
    AppContext.setStaged(0)
  }
}

export function getArchiveStateIcon(archiveState) {
  if ([ArchiveState.AIP, ArchiveState.SIP].includes(archiveState))
    return <Icon name="checkmark" color="green" />
  return null
}

export function getStepStatusIcon(stepStatus) {
  if ([StepStatus.FAILED, StepStatus.REJECTED].includes(stepStatus))
    return <Icon name="remove" color="red" />
  if ([StepStatus.COMPLETED].includes(stepStatus))
    return <Icon name="checkmark" color="green" />
  if (
    [
      StepStatus.WAITING,
      StepStatus.WAITING_APPROVAL,
      StepStatus.IN_PROGRESS,
    ].includes(stepStatus)
  )
    return <Icon loading name="spinner" color="blue" />
  return null
}

export const JSONTreeStyle = {
  scheme: 'preserve',
  author: '',
  base00: '#eff0f1', //background
  base08: '#21BA45', //null, undefined, symbol
  base09: '#21BA45', //number and bool
  base0B: '#0058af', //string, date
  base0D: '#000000', //json key, label
}

export const JSONTreeStyleFailed = {
  scheme: 'preserve',
  author: '',
  base00: '#eff0f1', //background
  base08: '#fb011e', //null, undefined, symbol
  base09: '#fb011e', //number and bool
  base0B: '#0058af', //string, date
  base0D: '#000000', //json key, label
}
