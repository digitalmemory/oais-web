import { api } from '@/api.js'
import { AppContext } from '@/AppContext.js'
import { archiveType, collectionType } from '@/types.js'
import {
  formatDateTime,
  sendNotification,
  ArchiveStateLabel,
  getArchiveStateIcon,
} from '@/utils.js'
import PropTypes from 'prop-types'
import React from 'react'
import { Button, Table, Segment, Grid, Label } from 'semantic-ui-react'
import { Link } from 'react-router-dom'
import _ from 'lodash'

export class JobArchives extends React.Component {
  static propTypes = {
    job: collectionType.isRequired,
  }

  render() {
    const { job } = this.props
    return (
      <Segment raised>
        <Label color="blue" ribbon>
          Job Archives
        </Label>
        <Grid>
          <Grid.Row>
            <Grid.Column floated="left">
              <h1>Archives</h1>
            </Grid.Column>
          </Grid.Row>
        </Grid>
        <CollectionArchivesList archives={job.archives} />
      </Segment>
    )
  }
}

export class CollectionArchivesList extends React.Component {
  static propTypes = {
    archives: PropTypes.arrayOf(archiveType).isRequired,
  }

  render() {
    const { archives } = this.props
    return (
      <Table textAlign="center">
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>ID</Table.HeaderCell>
            <Table.HeaderCell>Record</Table.HeaderCell>
            <Table.HeaderCell>Title</Table.HeaderCell>
            <Table.HeaderCell>Requester</Table.HeaderCell>
            <Table.HeaderCell>Approver</Table.HeaderCell>
            <Table.HeaderCell>Creation Date</Table.HeaderCell>
            <Table.HeaderCell>State</Table.HeaderCell>
            <Table.HeaderCell></Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {archives.map((archive) => (
            <Archive key={archive.id} archive={archive} />
          ))}
        </Table.Body>
      </Table>
    )
  }
}

class Archive extends React.Component {
  static propTypes = {
    archive: archiveType.isRequired,
  }

  constructor(props) {
    super(props)
    this.state = {
      loading: true,
      open: false,
      lastStep: null,
    }
  }

  componentDidMount() {
    this.setState({ loading: false })
  }

  static contextType = AppContext.Context

  render() {
    const { archive } = this.props

    return (
      <Table.Row>
        <Table.Cell>{archive.id}</Table.Cell>
        <Table.Cell>
          {archive.recid} ({archive.source})
        </Table.Cell>
        <Table.Cell>{archive.title}</Table.Cell>
        <Table.Cell>
          <Link to={`/users/${archive.requester.id}`}>
            {archive.requester.username}
          </Link>
        </Table.Cell>
        <Table.Cell>
          <Link to={`/users/${archive.approver?.id}`}>
            {archive.approver?.username}
          </Link>
        </Table.Cell>
        <Table.Cell>{formatDateTime(archive.timestamp)}</Table.Cell>
        <Table.Cell>
          <p>
            {ArchiveStateLabel[archive.state]}
            {getArchiveStateIcon(archive.state)}
          </p>
        </Table.Cell>
        <Table.Cell>
          <Link to={`/archive/${archive.id}`}>
            <Button>Details</Button>
          </Link>
        </Table.Cell>
      </Table.Row>
    )
  }
}
