import Harvest from '@/pages/Harvest/Harvest.jsx'
import { UploadSIP } from '@/pages/Upload/UploadSIP.jsx'
import { UploadFolder } from '@/pages/Upload/UploadFolder.jsx'
import { Announce } from '@/pages/Announce/Announce.jsx'
import URLParse from '@/pages/URLParse/URLParse.jsx'
import React from 'react'
import { Grid, Segment } from 'semantic-ui-react'
import { BatchAnnounce } from '../Announce/BatchAnnounce'
import { AppContext } from '@/AppContext.js'

/**
 * Renders the Add Resource page, containing two parts:
 * the Harvest and the Upload segments
 * The HarvestRedirect component is used to allow redirecting to the search
 * results instead of updating the local state.
 */
export class AddResource extends React.Component {
  static contextType = AppContext.Context

  render() {
    const { user } = this.context
    const isSuperUser = user && user.is_superuser

    return (
      <Grid>
        <Grid.Row>
          <Grid.Column>
            <h1>Add resource</h1>
            <p>
              Here you can find different ways to import data into the platform
              to start its long term preservation process.
            </p>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <Segment>
              <Harvest redirectURL="/harvest" />
            </Segment>
          </Grid.Column>
        </Grid.Row>

        <Grid.Row>
          <Grid.Column>
            <Segment>
              <URLParse />
            </Segment>
          </Grid.Column>
        </Grid.Row>

        {isSuperUser ? (
          <>
            <Grid.Row>
              <Grid.Column>
                <h1>Advanced features</h1>
                <p>
                  Here are some more advanced workflows to submit your data.
                </p>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column>
                <Segment>
                  <UploadFolder />
                </Segment>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column>
                <Segment>
                  <UploadSIP />
                </Segment>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column>
                <Segment>
                  <Announce />
                </Segment>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column>
                <Segment>
                  <BatchAnnounce />
                </Segment>
              </Grid.Column>
            </Grid.Row>
          </>
        ) : (
          <Grid.Row>
            <Grid.Column>
              <p>
                Advanced features are currently restricted for admins only,{' '}
                <a href="mailto:oais-admin@cern.ch">contact us</a> in case of
                any questions.
              </p>
            </Grid.Column>
          </Grid.Row>
        )}
      </Grid>
    )
  }
}
