import { api } from '@/api.js'
import { AppContext } from '@/AppContext.js'
import { archiveType, collectionTypeDetailed } from '@/types.js'
import {
  formatDateTime,
  sendNotification,
  ArchiveStateLabel,
  getArchiveStateIcon
} from '@/utils.js'
import PropTypes from 'prop-types'
import React from 'react'
import {
  Button,
  Table,
  Segment,
  Grid,
  Label,
  Modal,
  Header,
  Icon,
} from 'semantic-ui-react'
import { Link } from 'react-router-dom'
import { AddArchives } from '@/components/AddArchivesToCollection/AddArchives.jsx'
import _ from 'lodash'

export class CollectionArchives extends React.Component {
  static propTypes = {
    collection: collectionTypeDetailed.isRequired,
    onArchiveAdd: PropTypes.func.isRequired,
    onArchiveRemoval: PropTypes.func.isRequired,
    setRefresh: PropTypes.func.isRequired,
  }

  render() {
    const { collection } = this.props
    return (
      <Segment raised>
        <Label color="blue" ribbon>
          Tag Archives
        </Label>
        <Grid>
          <Grid.Row>
            <Grid.Column floated="left" width={10}>
              <h1>Archives</h1>
            </Grid.Column>
            <Grid.Column floated="right" width={3} textAlign="right">
              <AddArchives
                onArchiveAdd={this.props.onArchiveAdd}
                archives={collection.archives}
                collection={collection}
                setRefresh={this.props.setRefresh}
              />
            </Grid.Column>
          </Grid.Row>
        </Grid>
        <CollectionArchivesList
          archives={collection.archives}
          onArchiveRemoval={this.props.onArchiveRemoval}
          setRefresh={this.props.setRefresh}
        />
      </Segment>
    )
  }
}

export class CollectionArchivesList extends React.Component {
  static propTypes = {
    archives: PropTypes.arrayOf(archiveType).isRequired,
    onArchiveRemoval: PropTypes.func.isRequired,
    setRefresh: PropTypes.func.isRequired,
  }

  render() {
    const { archives } = this.props
    return (
      <Table textAlign="center">
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>ID</Table.HeaderCell>
            <Table.HeaderCell>Record</Table.HeaderCell>
            <Table.HeaderCell>Requester</Table.HeaderCell>
            <Table.HeaderCell>Approver</Table.HeaderCell>
            <Table.HeaderCell>State</Table.HeaderCell>
            <Table.HeaderCell>Creation Date</Table.HeaderCell>
            <Table.HeaderCell textAlign="right">Actions</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {archives.map((archive) => (
            <Archive
              key={archive.id}
              archive={archive}
              onArchiveRemoval={this.props.onArchiveRemoval}
              setRefresh={this.props.setRefresh}
            />
          ))}
        </Table.Body>
      </Table>
    )
  }
}

class Archive extends React.Component {
  static propTypes = {
    archive: archiveType.isRequired,
    onArchiveRemoval: PropTypes.func,
    setRefresh: PropTypes.func.isRequired,
  }

  constructor(props) {
    super(props)
    this.state = {
      loading: true,
      open: false,
    }
  }

  setOpen = (value) => {
    this.setState({ open: value })
    this.props.setRefresh(!value)
  }

  handleArchiveRemoval = () => {
    this.props.onArchiveRemoval([this.props.archive.id])
    this.setOpen(false)
  }

  static contextType = AppContext.Context

  render() {
    const { archive } = this.props

    let deleteModal = (
      <Modal
        closeIcon
        open={this.state.open}
        trigger={<Button icon="remove circle" color="red" />}
        onClose={() => this.setOpen(false)}
        onOpen={() => this.setOpen(true)}
      >
        <Header icon="archive" content="Remove this archive?" />
        <Modal.Content>
          <p>
            Are you sure you want to remove this archive from this collection?
          </p>
        </Modal.Content>
        <Modal.Actions>
          <Button color="red" onClick={() => this.setOpen(false)}>
            <Icon name="remove" /> No
          </Button>
          <Button color="green" onClick={() => this.handleArchiveRemoval()}>
            <Icon name="checkmark" /> Yes
          </Button>
        </Modal.Actions>
      </Modal>
    )

    return (
      <Table.Row>
        <Table.Cell>{archive.id}</Table.Cell>
        <Table.Cell>
          {archive.recid} ({archive.source})
        </Table.Cell>
        <Table.Cell>
          <Link to={`/users/${archive.requester?.id}`}>
            {archive.requester?.username}
          </Link>
        </Table.Cell>
        <Table.Cell>
          <Link to={`/users/${archive.approver?.id}`}>
            {archive.approver?.username}
          </Link>
        </Table.Cell>
        <Table.Cell>
          {ArchiveStateLabel[archive.state]} 
          {getArchiveStateIcon(archive.state)}
        </Table.Cell>
        <Table.Cell>{formatDateTime(archive.timestamp)}</Table.Cell>
        <Table.Cell textAlign="right">
          <Link to={`/archive/${archive.id}`}>
            <Button>Details</Button>
          </Link>
          {deleteModal}
        </Table.Cell>
      </Table.Row>
    )
  }
}
