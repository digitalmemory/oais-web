import PropTypes from 'prop-types'
import React from 'react'

export class ErrorPage extends React.Component {
  static propTypes = {
    header: PropTypes.string,
    text: PropTypes.string,
  }

  render() {
    return (
      <React.Fragment>
        <React.Fragment>
          <h1>{this.props.header ? this.props.header : 'Error'}</h1>
          <p>
            {this.props.text
              ? this.props.text
              : 'An error has occured, please try again later or contact the site admins.'}
          </p>
        </React.Fragment>
      </React.Fragment>
    )
  }
}

export class NotFoundPage extends React.Component {
  render() {
    const header = '404 - Page Not Found'
    const text =
      'The page you are looking for does not exist. Please try again later or contact the site admins.'
    return <ErrorPage header={header} text={text} />
  }
}

export class ForbiddenPage extends React.Component {
  render() {
    const header = '403 - Forbidden'
    const text =
      'You do not have the permissions to access the page you are looking for. To request access, please contact the site admins.'
    return <ErrorPage header={header} text={text} />
  }
}
