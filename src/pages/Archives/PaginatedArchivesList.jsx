import { ArchivesList } from '@/pages/Archives/ArchivesList.jsx'
import { sendNotification } from '@/utils.js'
import PropTypes from 'prop-types'
import { archiveType } from '@/types.js'
import React from 'react'
import { Link } from 'react-router-dom'
import {
  Loader,
  Form,
  Grid,
  Button,
  Icon,
  Accordion,
  Dropdown,
  Modal,
  Label,
  Input,
} from 'semantic-ui-react'
import {
  ArchiveState,
  ArchiveStateLabel,
  ArchiveStateOrder,
  ArchiveActionName,
  ArchiveActionLabel,
  StepName,
  hasPermission,
  StepNameLabel,
  StepStatus,
  StepStatusLabel,
  Permissions,
} from '@/utils.js'
import _ from 'lodash'
import { api } from '@/api.js'
import { AppContext } from '@/AppContext.js'
import { PaginationHeader } from '@/components/Pagination/PaginationHeader'
import { CreatePipeline } from '@/components/Pipeline/CreatePipeline'

const pageSizeOptions = [10, 20, 50, 100, 150]

/**
 * This component loads the archives and creates the pagination component
 * and the list of the archives.
 */
export class PaginatedArchivesList extends React.Component {
  static propTypes = {
    getArchives: PropTypes.func.isRequired,
    tagFilter: PropTypes.string,
  }

  constructor(props) {
    super(props)
    this.state = {
      archives: [],
      page: 1,
      totalArchives: 0,
      hitsPerPage: 20,
      loading: true,
      stateFilter: '',
      sourceFilter: '',
      tagFilter: props.tagFilter,
      stepNameFilter: '',
      stepStatusFilter: '',
      queryFilter: '',
      tags: [],
      activeIndex: 0,
      checkedList: [],
      open: false,
      actions: [],
      prevStateFilter: '',
      prevSourceFilter: '',
      prevTagFilter: props.tagFilter,
      prevStepNameFilter: '',
      prevStepStatusFilter: '',
      prevQueryFilter: '',
      canExecuteStep: false,
      sources: null,
      menuActions: [],
    }
  }

  handleArchiveUpdate = (newArchive) => {
    const archives = this.state.archives.map((archive) => {
      if (archive.id === newArchive.id) {
        return newArchive
      } else {
        return archive
      }
    })
    this.setState({ archives })
  }

  loadArchives = async (
    page = 1,
    size = this.state.hitsPerPage,
    stateFilter = this.state.stateFilter,
    sourceFilter = this.state.sourceFilter,
    tagFilter = this.state.tagFilter,
    stepNameFilter = this.state.stepNameFilter,
    stepStatusFilter = this.state.stepStatusFilter,
    queryFilter = this.state.queryFilter
  ) => {
    try {
      const { results: archives, count: totalArchives } =
        await this.props.getArchives(
          page,
          size,
          'all',
          stateFilter,
          sourceFilter,
          tagFilter,
          stepNameFilter,
          stepStatusFilter,
          queryFilter
        )
      if (page == 'all') {
        //Add all archives to checked list
        this.setState({ checkedList: archives })
      } else {
        this.setState({ archives, page, totalArchives })
        this.setState({
          prevSourceFilter: sourceFilter,
          prevStateFilter: stateFilter,
          prevStepNameFilter: stepNameFilter,
          prevStepStatusFilter: stepStatusFilter,
          prevTagFilter: tagFilter,
          prevQueryFilter: queryFilter,
        })
      }
    } catch (e) {
      sendNotification('Error while fetching archives', e.message, 'error')
    }
  }

  loadTags = async () => {
    try {
      let { result: tags } = await api.getAllTagNames()
      tags = _.map(tags, (tag) => ({
        key: tag.id,
        text: tag.title,
        value: tag.id,
      }))
      this.setState({ tags: tags })
    } catch (e) {
      sendNotification('Error while fetching tags', e.message, 'error')
    }
  }

  // This function gets called periodically to refresh the archive status and fetch updates
  refresh() {
    // Load the archives again in case something else got updated in the meantime
    this.loadArchives(
      this.state.page,
      this.state.hitsPerPage,
      this.state.prevStateFilter,
      this.state.prevSourceFilter,
      this.state.prevTagFilter,
      this.state.prevStepNameFilter,
      this.state.prevStepStatusFilter,
      this.state.prevQueryFilter
    )
  }

  refreshInterval = null

  componentDidMount(props) {
    this.loadArchives()
    this.loadTags()
    this.getSources()
    this.refreshInterval = setInterval(() => this.refresh(), 10000)
    this.setState({ loading: false })
    const { user } = this.context
    if (hasPermission(user, Permissions.CAN_EXECUTE_STEP)) {
      this.setState({ canExecuteStep: true })
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.tagFilter !== prevProps.tagFilter) {
      this.setState({ loading: true })
      this.loadArchives()
      this.loadTags()
      this.setState({ loading: false })
    }
  }

  componentWillUnmount() {
    // Once we're done, stop with period call for refreshes
    clearInterval(this.refreshInterval)
  }

  handleSubmit = (event) => {
    event.preventDefault()
    this.loadArchives()
  }

  handleStateFilterChange = (event, { value }) => {
    this.setState({ stateFilter: value })
  }

  handleSourceChange = (event, { value }) => {
    this.setState({ sourceFilter: value })
  }

  handleTagChange = (event, { value }) => {
    this.setState({ tagFilter: value })
  }

  handleStepNameChange = (event, { value }) => {
    this.setState({ stepNameFilter: value })
  }

  handleStepStatusChange = (event, { value }) => {
    this.setState({ stepStatusFilter: value })
  }

  handleQueryChange = (event, { value }) => {
    this.setState({ queryFilter: value })
  }

  handleClear = async (event) => {
    event.preventDefault()
    if (this.isAnyFilterSet()) {
      this.setState(
        {
          sourceFilter: '',
          stateFilter: '',
          stepNameFilter: '',
          stepStatusFilter: '',
          tagFilter: '',
          queryFilter: '',
          prevSourceFilter: '',
          prevStateFilter: '',
          prevStepNameFilter: '',
          prevStepStatusFilter: '',
          prevTagFilter: '',
          prevQueryFilter: '',
          loading: true,
        },
        async () => {
          await this.loadArchives()
          this.setState({ loading: false })
        }
      )
    }
  }

  handleClick = (e, titleProps) => {
    const { index } = titleProps
    const { activeIndex } = this.state
    const newIndex = activeIndex === index ? -1 : index

    this.setState({ activeIndex: newIndex })
  }

  handleSelectAllOnPage = (event) => {
    event.preventDefault()
    let newCheckedList = this.state.checkedList
    this.state.archives.forEach((archive) => {
      const exists = this.state.checkedList.some((el) => el.id == archive.id)
      if (!exists) newCheckedList = newCheckedList.concat(archive)
    })
    this.setState({ checkedList: newCheckedList })
  }

  handleSelectAll = (event) => {
    event.preventDefault()
    this.loadArchives(
      'all',
      this.state.hitsPerPage,
      this.state.prevStateFilter,
      this.state.prevSourceFilter,
      this.state.prevTagFilter,
      this.state.prevStepNameFilter,
      this.state.prevStepStatusFilter,
      this.state.prevQueryFilter
    )
  }

  handleDeselectAll = (event) => {
    event.preventDefault()
    this.setState({ checkedList: [] })
  }

  handlePageSizeChange = (size = 20) => {
    this.setState({
      hitsPerPage: Number(size),
    })

    this.loadArchives(
      1,
      size,
      this.state.prevStateFilter,
      this.state.prevSourceFilter,
      this.state.prevTagFilter,
      this.state.prevStepNameFilter,
      this.state.prevStepStatusFilter,
      this.state.prevQueryFilter
    )
  }

  addRecord = (archive) => {
    this.setState({ checkedList: this.state.checkedList.concat(archive) })
  }

  removeRecord = (archive) => {
    const newCheckedList = this.state.checkedList.filter(
      (x) => x.id !== archive.id
    )
    this.setState({ checkedList: newCheckedList })
  }

  batchExecute = () => {
    const { actions } = this.state
    _.map(this.state.checkedList, function (archive) {
      try {
        if (actions[0] == ArchiveActionName.RERUN_FAILED) {
          return api.archiveRunPipeline(archive, 'retry')
        } else if (actions[0] == ArchiveActionName.CONTINUE_PIPELINE) {
          return api.archiveRunPipeline(archive, 'continue')
        } else {
          return api.archiveRunPipeline(archive, 'run', actions)
        }
      } catch (e) {
        sendNotification('Error while executing pipeline', e.message, 'error')
      }
    })
    this.setState(
      { page: 1, open: false, actions: [], checkedList: [] },
      () => {
        this.refresh()
      }
    )
  }

  setBatchAction = (actions) => {
    this.setState({ actions: actions, open: true })
  }

  onClose = () => this.setState({ open: false })

  static contextType = AppContext.Context

  isAnyFilterSet = () => {
    return (
      (
        this.state.prevSourceFilter ||
        this.state.prevStateFilter ||
        this.state.prevTagFilter ||
        this.state.prevStepNameFilter ||
        this.state.prevStepStatusFilter ||
        this.state.prevQueryFilter
      ).toString().length > 0
    )
  }

  getSources = async () => {
    const sources = await api.getArchivesSources()
    this.setState({ sources: sources })
  }

  render() {
    const { archives, page, totalArchives, loading, hitsPerPage } = this.state

    const loadingSpinner = <Loader inverted>Loading</Loader>

    let stateOptions = _.map(ArchiveState, (value) => ({
      key: value,
      text: ArchiveStateLabel[value],
      value: value,
    }))
    stateOptions = _.sortBy(stateOptions, (x) => ArchiveStateOrder[x.key])

    let stepNameOptions = _.map(StepName, (value) => ({
      key: value,
      text: StepNameLabel[value],
      value: value,
    }))

    let stepStatusOptions = _.map(StepStatus, (value) => ({
      key: value,
      text: StepStatusLabel[value],
      value: value,
    }))

    let sourceOptions = _.map(this.state.sources, (source) => ({
      key: source,
      text: source,
      value: source,
    }))

    return (
      <div>
        <Accordion>
          <Accordion.Title
            active={this.state.activeIndex === 0}
            index={0}
            onClick={this.handleClick.bind(this)}
          >
            <Icon name="dropdown" />
            <h2 style={{ display: 'inline-block', marginTop: '5px' }}>
              Filter
            </h2>
          </Accordion.Title>
          <Accordion.Content active={this.state.activeIndex === 0}>
            <Form onSubmit={this.handleSubmit.bind(this)}>
              <Grid stackable columns={4}>
                <Grid.Row>
                  <Grid.Column verticalAlign="bottom" width={8}>
                    <Form.Field
                      control={Input}
                      action
                      value={this.state.queryFilter}
                      onChange={this.handleQueryChange.bind(this)}
                      label="Query"
                      placeholder="Title or record ID"
                      icon={
                        <Icon
                          name="delete"
                          link
                          onClick={(event) =>
                            this.handleQueryChange.bind(this)(event, {
                              value: '',
                            })
                          }
                        />
                      }
                    />
                  </Grid.Column>
                  <Grid.Column verticalAlign="bottom" width={4}>
                    <Form.Select
                      label="Source"
                      onChange={this.handleSourceChange.bind(this)}
                      value={this.state.sourceFilter}
                      options={sourceOptions}
                      placeholder="Source"
                      clearable={true}
                    />
                  </Grid.Column>
                  <Grid.Column verticalAlign="bottom" width={4}>
                    <Form.Select
                      label="Tag"
                      onChange={this.handleTagChange.bind(this)}
                      value={this.state.tagFilter}
                      options={this.state.tags}
                      search
                      placeholder="Tag"
                      clearable={true}
                    />
                  </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column verticalAlign="bottom" width={4}>
                    <Form.Select
                      label="Package state"
                      onChange={this.handleStateFilterChange.bind(this)}
                      value={this.state.stateFilter}
                      options={stateOptions}
                      placeholder="State"
                      clearable={true}
                    />
                  </Grid.Column>
                  <Grid.Column verticalAlign="bottom" width={4}>
                    <Form.Select
                      label="Last Step"
                      onChange={this.handleStepNameChange.bind(this)}
                      value={this.state.stepNameFilter}
                      options={stepNameOptions}
                      placeholder="Step"
                      clearable={true}
                    />
                  </Grid.Column>
                  <Grid.Column verticalAlign="bottom" width={4}>
                    <Form.Select
                      label="Last Step Status"
                      onChange={this.handleStepStatusChange.bind(this)}
                      value={this.state.stepStatusFilter}
                      options={stepStatusOptions}
                      placeholder="Step Status"
                      clearable={true}
                    />
                  </Grid.Column>
                </Grid.Row>
                <Grid.Row
                  verticalAlign="bottom"
                  style={{ justifyContent: 'flex-end' }}
                >
                  <Grid.Column verticalAlign="bottom" width={3}>
                    <Button primary fluid>
                      <Icon name="search" />
                      Filter
                    </Button>
                  </Grid.Column>
                  <Grid.Column width={3}>
                    <Button
                      type="button"
                      onClick={this.handleClear.bind(this)}
                      negative
                      fluid
                    >
                      <Icon name="cancel" />
                      Clear
                    </Button>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </Form>
          </Accordion.Content>
        </Accordion>

        {this.isAnyFilterSet() ? (
          <React.Fragment>
            <div
              style={{
                marginTop: '15px',
                marginRight: '10px',
                display: 'inline-block',
              }}
            >
              <h3>Filters used:</h3>{' '}
            </div>
            {this.state.prevQueryFilter && (
              <Label color="blue" size="large">
                {'Query: ' + this.state.prevQueryFilter}
              </Label>
            )}

            {this.state.prevSourceFilter && (
              <Label color="blue" size="large">
                {this.state.prevSourceFilter}
              </Label>
            )}
            {this.state.prevTagFilter && this.state.tags.length > 0 && (
              <Label color="blue" size="large">
                {
                  this.state.tags.find(
                    (item) => item.key == this.state.prevTagFilter
                  ).text
                }{' '}
              </Label>
            )}
            {this.state.prevStateFilter && (
              <Label color="blue" size="large">
                {ArchiveStateLabel[this.state.prevStateFilter]}
              </Label>
            )}
            {this.state.prevStepNameFilter && (
              <Label color="blue" size="large">
                {StepNameLabel[this.state.prevStepNameFilter]}
              </Label>
            )}
            {this.state.prevStepStatusFilter && (
              <Label color="blue" size="large">
                {StepStatusLabel[this.state.prevStepStatusFilter]}
              </Label>
            )}
          </React.Fragment>
        ) : null}

        {loading || totalArchives == 0 || totalArchives == null ? (
          <div> {loadingSpinner} </div>
        ) : (
          <div>
            <PaginationHeader
              hasResults={totalArchives != null && totalArchives > 0}
              activePage={page}
              onPageSearch={this.loadArchives}
              onPageSizeChange={this.handlePageSizeChange.bind(this)}
              totalNumHits={totalArchives}
              hitsPerPage={hitsPerPage}
              sizeSelection={pageSizeOptions}
            />
            <SelectionControls
              onSelectPage={this.handleSelectAllOnPage.bind(this)}
              onSelectAll={this.handleSelectAll.bind(this)}
              onDeselectAll={this.handleDeselectAll.bind(this)}
              onActionCancellation={this.onClose.bind(this)}
              onActionConfirmation={this.batchExecute.bind(this)}
              onActionSelection={this.setBatchAction.bind(this)}
              selection={this.state.checkedList}
              menuIsOpen={this.state.open}
              canExecuteStep={this.state.canExecuteStep}
              totalNumHits={totalArchives}
              hitsPerPage={hitsPerPage}
            />

            <ArchivesList
              archives={archives}
              onArchiveUpdate={this.handleArchiveUpdate}
              addRecord={this.addRecord.bind(this)}
              removeRecord={this.removeRecord.bind(this)}
              checkedList={this.state.checkedList}
            />

            {this.state.hitsPerPage > 10 && archives.length > 10 && (
              <>
                <PaginationHeader
                  hasResults={totalArchives != null && totalArchives > 0}
                  activePage={page}
                  onPageSearch={this.loadArchives}
                  onPageSizeChange={this.handlePageSizeChange.bind(this)}
                  totalNumHits={totalArchives}
                  hitsPerPage={hitsPerPage}
                  hideResultsMessage={true}
                  sizeSelection={pageSizeOptions}
                />
                <SelectionControls
                  onSelectPage={this.handleSelectAllOnPage.bind(this)}
                  onSelectAll={this.handleSelectAll.bind(this)}
                  onDeselectAll={this.handleDeselectAll.bind(this)}
                  onActionCancellation={this.onClose.bind(this)}
                  onActionConfirmation={this.batchExecute.bind(this)}
                  onActionSelection={this.setBatchAction.bind(this)}
                  selection={this.state.checkedList}
                  menuIsOpen={this.state.open}
                  canExecuteStep={this.state.canExecuteStep}
                  totalNumHits={totalArchives}
                  hitsPerPage={hitsPerPage}
                />
              </>
            )}
          </div>
        )}
        {totalArchives == 0 && (
          <div style={{ marginTop: '10px' }}>
            {' '}
            No archives found. <Link to="add-resource">Add some</Link> or browse
            the public ones on the{' '}
            <a
              href="https://oais-registry.web.cern.ch/"
              rel="noreferrer"
              target="_blank"
            >
              Digital Memory registry
            </a>
            .
          </div>
        )}
      </div>
    )
  }
}

class SelectionControls extends React.Component {
  static propTypes = {
    onSelectPage: PropTypes.func.isRequired,
    onSelectAll: PropTypes.func.isRequired,
    onDeselectAll: PropTypes.func.isRequired,
    onActionCancellation: PropTypes.func.isRequired,
    onActionConfirmation: PropTypes.func.isRequired,
    onActionSelection: PropTypes.func.isRequired,
    selection: PropTypes.arrayOf(archiveType),
    menuIsOpen: PropTypes.bool.isRequired,
    canExecuteStep: PropTypes.bool.isRequired,
    totalNumHits: PropTypes.number,
    hitsPerPage: PropTypes.number.isRequired,
  }

  constructor(props) {
    super(props)
    this.state = {
      menuActions: [],
      stepIntersection: [],
      loading: false,
      pipelineModalIsOpen: false,
    }
  }

  loadDropdownActions = async () => {
    try {
      this.setState({ loading: true })
      let actions = await api.getArchivesActions(this.props.selection)
      let intersection = actions['next_steps_intersection']
      this.setState({ stepIntersection: [...intersection] })

      // allow creation of a pipeline for selected archives with the same state
      if (actions['state_intersection'] == true) {
        intersection.push(ArchiveActionName.PIPELINE)
      }

      if (actions['all_last_step_failed'] == true) {
        intersection.push(ArchiveActionName.RERUN_FAILED)
      }

      if (actions['can_continue'] == true) {
        intersection.push(ArchiveActionName.CONTINUE_PIPELINE)
      }

      this.setState({ menuActions: intersection, loading: false })
    } catch (e) {
      sendNotification('Error while fetching actions', e.message, 'error')
      this.setState({ loading: false })
    }
  }

  onOpenPipelineModal = async () => {
    this.setState({ pipelineModalIsOpen: true })
  }

  onClosePipelineModal = async () => {
    this.setState({ pipelineModalIsOpen: false })
  }

  getDropdownActions = () => {
    let dropDownItems = []
    if (this.state.menuActions && this.state.menuActions.length > 0) {
      this.state.menuActions.forEach((action) => {
        dropDownItems.push(
          <Dropdown.Item
            text={ArchiveActionLabel[action]}
            onClick={() =>
              action === ArchiveActionName.PIPELINE
                ? this.onOpenPipelineModal()
                : this.props.onActionSelection([action])
            }
          />
        )
      })
      return dropDownItems
    } else {
      return <Dropdown.Item text="No possible action" />
    }
  }

  render() {
    return (
      <div style={{ marginTop: '10px', marginBottom: '10px' }}>
        <React.Fragment>
          <Button.Group>
            <Button onClick={this.props.onSelectPage}>
              Select all on page
            </Button>
            <Button.Or />
            {this.props.totalNumHits > this.props.hitsPerPage && (
              <React.Fragment>
                <Button onClick={this.props.onSelectAll}>
                  Select all {this.props.totalNumHits} results
                </Button>
                <Button.Or />
              </React.Fragment>
            )}
            <Button
              onClick={this.props.onDeselectAll}
              disabled={
                this.props.selection == null || this.props.selection.length <= 0
              }
            >
              Deselect all
            </Button>
          </Button.Group>
          {this.props.canExecuteStep && (
            <Dropdown
              text={'Action on ' + this.props.selection.length + ' selected'}
              button={true}
              style={{ float: 'right' }}
              onClick={() => this.loadDropdownActions()}
              loading={this.state.loading}
            >
              {!this.state.loading ? (
                <Dropdown.Menu>{this.getDropdownActions()}</Dropdown.Menu>
              ) : (
                <></>
              )}
            </Dropdown>
          )}
          <CreatePipeline
            initNextSteps={this.state.stepIntersection}
            open={this.state.pipelineModalIsOpen}
            onCreatePipeline={this.props.onActionSelection}
            onClose={this.onClosePipelineModal}
          />
          <Modal open={this.props.menuIsOpen}>
            <Modal.Header>Confirm action</Modal.Header>
            <Modal.Content image>
              <Modal.Description>
                <p>
                  Please note that the action will be executed on all selected
                  records(<b>{this.props.selection.length}</b>).
                </p>
              </Modal.Description>
            </Modal.Content>
            <Modal.Actions>
              <Button onClick={this.props.onActionCancellation}>Cancel</Button>
              <Button positive onClick={this.props.onActionConfirmation}>
                Confirm
              </Button>
            </Modal.Actions>
          </Modal>
        </React.Fragment>
      </div>
    )
  }
}
