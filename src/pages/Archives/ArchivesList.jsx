import { AppContext } from '@/AppContext.js'
import { archiveType } from '@/types.js'
import {
  StepStatusLabel,
  StepNameLabel,
  formatDateTime,
  ArchiveStateLabel,
  getArchiveStateIcon,
  getStepStatusIcon,
  StepDescription,
  ArchiveStateDescription,
} from '@/utils.js'
import PropTypes from 'prop-types'
import React from 'react'
import { Button, Table, Label, Checkbox, Popup } from 'semantic-ui-react'
import { Link } from 'react-router-dom'
import _ from 'lodash'

export class ArchivesList extends React.Component {
  static propTypes = {
    archives: PropTypes.arrayOf(archiveType).isRequired,
    onArchiveUpdate: PropTypes.func.isRequired,
    addRecord: PropTypes.func.isRequired,
    removeRecord: PropTypes.func.isRequired,
    checkedList: PropTypes.arrayOf(archiveType).isRequired,
  }

  render() {
    const { archives, onArchiveUpdate } = this.props
    return (
      <Table>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Select</Table.HeaderCell>
            <Table.HeaderCell>Last Update</Table.HeaderCell>
            <Table.HeaderCell>Original Record</Table.HeaderCell>
            <Table.HeaderCell width={6}>Title</Table.HeaderCell>
            <Table.HeaderCell>State</Table.HeaderCell>
            <Table.HeaderCell>Last Step</Table.HeaderCell>
            <Table.HeaderCell></Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {archives.map((archive) => (
            <Archive
              key={archive.id}
              archive={archive}
              onArchiveUpdate={onArchiveUpdate}
              isChecked={this.props.checkedList.some(
                (checkedRecord) => checkedRecord.id == archive.id
              )}
              addRecord={this.props.addRecord}
              removeRecord={this.props.removeRecord}
            />
          ))}
        </Table.Body>
      </Table>
    )
  }
}

class Archive extends React.Component {
  static propTypes = {
    archive: archiveType.isRequired,
    onArchiveUpdate: PropTypes.func.isRequired,
    isChecked: PropTypes.bool.isRequired,
    addRecord: PropTypes.func.isRequired,
    removeRecord: PropTypes.func.isRequired,
  }

  componentDidUpdate(prevProps) {
    if (this.props.isChecked !== prevProps.isChecked) {
      this.setState({ isChecked: this.props.isChecked })
    }
  }

  constructor(props) {
    super(props)
    this.state = {
      loading: false,
      isChecked: this.props.isChecked,
    }
  }

  static contextType = AppContext.Context

  handleChecked = (e, { id, checked }) => {
    this.setState({ isChecked: checked })
    if (checked) {
      this.props.addRecord(this.props.archive)
    } else {
      this.props.removeRecord(this.props.archive)
    }
  }

  getOriginalRecordElement = () => {
    if (this.props.archive.source_url != 'N/A') {
      return (
        <Link
          to={{ pathname: this.props.archive.source_url }}
          target="_blank"
          rel="noopener noreferrer"
        >
          <code>{this.props.archive.recid}</code> ({this.props.archive.source})
        </Link>
      )
    }
    return (
      <Popup
        trigger={
          <div>
            <code>{this.props.archive.recid}</code> ({this.props.archive.source}
            )
          </div>
        }
        content={'Original Record url not available'}
        inverted
        position="right center"
      />
    )
  }

  render() {
    const { archive } = this.props
    const { user } = this.context

    let showTitle = ''
    if (archive.title) {
      showTitle = archive.title
    }

    return (
      <Table.Row>
        <Table.Cell textAlign="center">
          <Checkbox
            id={archive.id}
            checked={this.state.isChecked}
            onClick={this.handleChecked}
          />
        </Table.Cell>
        <Table.Cell>{formatDateTime(archive.last_update)}</Table.Cell>
        <Table.Cell>{this.getOriginalRecordElement()}</Table.Cell>
        <Table.Cell>
          {showTitle}{' '}
          {!archive.restricted && (
            <Label size="mini" circular color="blue">
              PUBLIC
            </Label>
          )}
        </Table.Cell>
        <Table.Cell>
          <Popup
            trigger={
              <div>
                {ArchiveStateLabel[archive.state]}{' '}
                {getArchiveStateIcon(archive.state)}
              </div>
            }
            content={<div>{ArchiveStateDescription[archive.state]}</div>}
            inverted
            position="right center"
          />
        </Table.Cell>
        <Table.Cell>
          <Popup
            trigger={
              <div>
                {StepNameLabel[archive.last_step?.name]}{' '}
                {getStepStatusIcon(archive.last_step?.status)}
              </div>
            }
            content={
              <div>
                {StepDescription[archive.last_step?.name]} :{' '}
                {StepStatusLabel[archive.last_step?.status]}
              </div>
            }
            inverted
            position="right center"
          />
        </Table.Cell>
        <Table.Cell>
          <Button
            as="a"
            href={`/archive/${archive.id}`}
            target="_blank"
            rel="noopener noreferrer"
          >
            Details
          </Button>
        </Table.Cell>
      </Table.Row>
    )
  }
}
