import { api } from '@/api.js'
import { AppContext } from '@/AppContext.js'
import { stepType, archiveType } from '@/types.js'
import {
  StepStatus,
  StepStatusLabel,
  StepNameLabel,
  formatDateTime,
  sendNotification,
  JSONTreeStyle,
  JSONTreeStyleFailed,
} from '@/utils.js'
import PropTypes from 'prop-types'
import React from 'react'
import { Button, Icon, Grid, Accordion, Container } from 'semantic-ui-react'
import { ActionButton } from '../../components/Pipeline/StepElement'
import { JSONTree } from 'react-json-tree'

/**
 * This component gets the steps as a prop and renders a list of the steps
 * using the map function.
 *
 */
export class StepsList extends React.Component {
  static propTypes = {
    steps: PropTypes.arrayOf(stepType),
    archive: archiveType.isRequired,
  }

  render() {
    const { steps, archive } = this.props

    return (
      <React.Fragment>
        {steps.map((step) => (
          <Step
            key={step.id}
            step={step}
            archive={archive}
            lastStep={archive.last_step}
          />
        ))}
      </React.Fragment>
    )
  }
}

/**
 * This component gets each step from the map function of the parent component
 * and renders all relevant information.
 *
 */
class Step extends React.Component {
  static propTypes = {
    step: stepType.isRequired,
    archive: archiveType.isRequired,
    lastStep: stepType.isRequired,
  }
  state = {
    activeIndex: 0,
    loading: false,
    output: null,
    artifact: null,
    renderDownloadButton: false,
    renderArchivematicaDetails: false,
  }

  handleClick = (e, titleProps) => {
    const { index } = titleProps
    const { activeIndex } = this.state
    const newIndex = activeIndex === index ? -1 : index

    this.setState({ activeIndex: newIndex })
  }

  componentDidMount() {
    this.setOutput(() => {
      this.loadArchivematicaDetails()
      this.loadDownloadButton()
    })
  }

  static contextType = AppContext.Context

  approve = async () => {
    const { step } = this.props
    try {
      await api.approveArchive(step.id)
      this.setState({ loading: true })
    } catch (e) {
      sendNotification('Error while approving archive', e.message, 'error')
    }
  }

  reject = async () => {
    const { step } = this.props
    try {
      await api.rejectArchive(step.id)
      this.setState({ loading: true })
    } catch (e) {
      sendNotification('Error while rejecting archive', e.message, 'error')
    }
  }

  handleRunStep = async (runType) => {
    try {
      await api.archiveRunPipeline(this.props.archive, runType)
    } catch (e) {
      sendNotification('Error while running pipeline', e.message, 'error')
    }
  }

  setOutput = (callback) => {
    const step = this.props.step
    if (step.output_data !== null && step.output_data.length > 0) {
      try {
        // For Archivematic output in order to display the JSON correctly double quotes must be replaced with single quotes
        if (step.name === 5) {
          step.output_data = step.output_data.replaceAll("'", '"')
        }
        var output = JSON.parse(step.output_data)
        this.setState({ output: output }, callback)
      } catch (e) {
        sendNotification('Error while parsing step output data', e.message)
        this.setState({ output: step.output_data })
      }
    }
  }

  loadArchivematicaDetails = () => {
    if (this.props.step.name === 5 && this.state.output !== null) {
      this.setState({ renderArchivematicaDetails: true })
    }
  }

  loadDownloadButton = () => {
    if (this.state.output !== null && this.state.output.artifact) {
      this.setState({
        artifact: this.state.output.artifact,
        renderDownloadButton: true,
      })
    }
  }

  render() {
    const { archive, step, lastStep } = this.props
    const { user } = this.context
    const {
      activeIndex,
      loading,
      renderDownloadButton,
      renderArchivematicaDetails,
      artifact,
      output,
    } = this.state

    let actionIcon = null
    let onClick = null

    if (step.status === StepStatus.FAILED && step.id === lastStep.id) {
      actionIcon = 'redo'
      onClick = () => this.handleRunStep('retry')
    } else if (
      step.status == StepStatus.WAITING &&
      archive.last_step &&
      archive.last_step.id == step.input_step &&
      archive.last_step.status == StepStatus.FAILED
    ) {
      // step is the first (not run) step after the last failed one
      actionIcon = 'play'
      onClick = () => this.handleRunStep('continue')
    }

    let showFailedDetails = null
    let JSONStyle = JSONTreeStyle
    if (step.status === StepStatus.FAILED && this.state.output) {
      if (this.state.output.errormsg) {
        JSONStyle = JSONTreeStyleFailed
        showFailedDetails = (
          <Grid.Row>
            <Grid.Column>
              <b>Error message: </b> {this.state.output.errormsg}
            </Grid.Column>
          </Grid.Row>
        )
      }
    }

    let downloadButton = null
    if (artifact && renderDownloadButton) {
      let downloadLink = null
      let iconName = null
      switch (artifact.artifact_name) {
        case 'Invenio Link':
        case 'FTS Job':
          downloadLink = artifact.artifact_url
          iconName = 'external alternate'
          break
        case 'SIP':
        case 'AIP':
          downloadLink = '/api/steps/' + step.id + '/download-artifact'
          iconName = 'download'
          break
      }
      if (downloadLink && iconName) {
        downloadButton = (
          <Button
            size="tiny"
            color="blue"
            as="a"
            href={downloadLink}
            target="_blank"
          >
            <Icon name={iconName}></Icon>
            {artifact.artifact_name}
          </Button>
        )
      }
    }

    var artifactText = <span>Artifacts:&nbsp;</span>
    let accordionActions = null
    if (downloadButton) {
      accordionActions = [artifactText, downloadButton]
    }

    let output_data = this.state.output

    return (
      <Container>
        <Grid>
          <Grid.Row columns={2}>
            <Grid.Column floated="left" width={5}>
              <h3>{StepNameLabel[step.name]}</h3>
            </Grid.Column>
            <Grid.Column floated="right" textAlign="right">
              {accordionActions && <>{accordionActions}</>}
            </Grid.Column>
          </Grid.Row>
        </Grid>
        <Accordion fluid styled>
          <Accordion.Title
            active={activeIndex === 0}
            index={0}
            onClick={this.handleClick}
          >
            <Icon name="dropdown" />
            Step Details
          </Accordion.Title>
          <Accordion.Content active={activeIndex === 0}>
            <Grid>
              <Grid.Row columns={4}>
                <Grid.Column>
                  <b>ID: </b> {step.id}
                </Grid.Column>
                <Grid.Column>
                  <b>Start Date: </b>{' '}
                  {step.start_date != null && formatDateTime(step.start_date)}
                </Grid.Column>
                <Grid.Column>
                  <b>End Date: </b>{' '}
                  {step.finish_date != null && formatDateTime(step.finish_date)}
                </Grid.Column>
                <Grid.Column>
                  <b>Status: </b> {StepStatusLabel[step.status]}{' '}
                  {actionIcon && ActionButton[actionIcon](onClick, step.id)}
                </Grid.Column>
              </Grid.Row>
              {step.status == 3 && showFailedDetails}
            </Grid>
          </Accordion.Content>
          <Accordion.Title
            active={activeIndex === 1}
            index={1}
            onClick={this.handleClick}
          >
            <Icon name="dropdown" />
            Celery Task Details
          </Accordion.Title>
          <Accordion.Content active={activeIndex === 1}>
            <Grid>
              <Grid.Row columns={1}>
                <Grid.Column>
                  <b>Task ID: </b> {step.celery_task_id}
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Accordion.Content>
          <Accordion.Title
            active={activeIndex === 3}
            index={3}
            onClick={this.handleClick}
          >
            <Icon name="dropdown" />
            Output data
          </Accordion.Title>
          <Accordion.Content active={activeIndex === 3}>
            <JSONTree
              data={output_data}
              hideRoot={true}
              shouldExpandNodeInitially={() => true}
              theme={JSONStyle}
            />
          </Accordion.Content>
          {renderArchivematicaDetails && (
            <React.Fragment>
              <Accordion.Title
                active={activeIndex === 2}
                index={2}
                onClick={this.handleClick}
              >
                <Icon name="dropdown" />
                Archivematica Details
              </Accordion.Title>
              <Accordion.Content active={activeIndex === 2}>
                <Grid>
                  <Grid.Row columns={3}>
                    <Grid.Column>
                      <b>Step: </b> {output.type}
                    </Grid.Column>
                    <Grid.Column>
                      <b>UUID: </b> {output.uuid}
                    </Grid.Column>
                    <Grid.Column>
                      <b>Status: </b> {output.status}
                    </Grid.Column>
                  </Grid.Row>
                  <Grid.Row columns={2}>
                    <Grid.Column>
                      <b>Directory: </b> {output.directory}
                    </Grid.Column>
                    <Grid.Column>
                      <b>File Name: </b> {output.name}
                    </Grid.Column>
                  </Grid.Row>
                  <Grid.Row columns={2}>
                    <Grid.Column>
                      <b>Microservice: </b> {output.microservice}
                    </Grid.Column>
                    <Grid.Column>
                      <b>Message: </b> {output.message}
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
              </Accordion.Content>
            </React.Fragment>
          )}
        </Accordion>
        <br />
      </Container>
    )
  }
}
