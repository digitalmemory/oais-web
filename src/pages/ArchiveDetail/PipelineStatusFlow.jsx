import { api } from '@/api.js'
import { AppContext } from '@/AppContext.js'
import { stepType, archiveType } from '@/types.js'
import {
  sendNotification,
  StepStatus,
  Permissions,
  hasPermission,
} from '@/utils.js'
import PropTypes from 'prop-types'
import React from 'react'
import { Segment, Label } from 'semantic-ui-react'
import { StepElement } from '@/components/Pipeline/StepElement.jsx'
import { AddStepToPipeline } from '@/components/Pipeline/AddStepToPipeline.jsx'
/**
 * This component shows a flow of the completed or pending steps.
 * It gets a list of steps and the archive details as props and
 * renders a grid of circular segments one for each step
 *
 */
export class StepsPipeline extends React.Component {
  static propTypes = {
    steps: PropTypes.arrayOf(stepType),
    archive: archiveType.isRequired,
  }

  constructor(props) {
    super(props)
  }

  render() {
    const { archive, steps } = this.props

    return (
      <Segment raised>
        <Label color="blue" ribbon>
          Pipeline overview
        </Label>
        <PipelineStatus steps={steps} archive={archive} />
      </Segment>
    )
  }
}

/**
 * This component creates the grid for the steps elements and
 * creates another one segment for the selection of the next step
 * if the nextSteps array is not null
 *
 */
class PipelineStatus extends React.Component {
  static propTypes = {
    steps: PropTypes.arrayOf(stepType),
    archive: archiveType.isRequired,
  }

  constructor(props) {
    super(props)
    this.state = {
      nextSteps: [],
    }
  }

  static contextType = AppContext.Context

  loadArchiveNextSteps = async () => {
    try {
      const nextSteps = await api.getArchiveNextSteps(this.props.archive.id)
      this.setState({ nextSteps: nextSteps })
    } catch (e) {
      sendNotification(
        "Error while fetching archive's next steps",
        e.message,
        'error'
      )
      throw new Error(e.message)
    }
  }

  onStepRetry = async () => {
    try {
      await api.archiveRunPipeline(this.props.archive, 'retry')
      await this.loadArchiveNextSteps(this.props.archive.id)
    } catch (e) {
      sendNotification('Error while retrying step', e.message, 'error')
    }
  }

  onStepContinue = async () => {
    try {
      await api.archiveRunPipeline(this.props.archive, 'continue')
      await this.loadArchiveNextSteps(this.props.archive.id)
    } catch (e) {
      sendNotification('Error while continuing pipeline', e.message, 'error')
    }
  }

  onAddStep = async (event, { value }) => {
    try {
      await api.archiveRunPipeline(this.props.archive, 'run', [value])
      await this.loadArchiveNextSteps(this.props.archive.id)
    } catch (e) {
      sendNotification('Error while executing pipeline', e.message, 'error')
    }
  }

  onAddPipeline = async (pipeline) => {
    try {
      await api.archiveRunPipeline(this.props.archive, 'run', pipeline)
      await this.loadArchiveNextSteps(this.props.archive.id)
    } catch (e) {
      sendNotification('Error while executing pipeline', e.message, 'error')
    }
  }

  async componentDidMount() {
    await this.loadArchiveNextSteps()
  }

  render() {
    const { steps, archive } = this.props
    const { nextSteps } = this.state
    const { user } = this.context

    let canExecuteStep = false
    if (hasPermission(user, Permissions.CAN_EXECUTE_STEP)) {
      canExecuteStep = true
    }

    let hidden_steps = []
    steps.forEach((step) => {
      if (
        step.status === StepStatus.FAILED &&
        step.id !== archive.last_step.id
      ) {
        const successors = steps.filter(
          (next_step) =>
            next_step.name === step.name &&
            next_step.status !== StepStatus.WAITING &&
            next_step.input_step == step.id
        )
        if (successors.length != 0) {
          hidden_steps.push(step.id)
        }
      }
    })

    const visibleSteps = steps.filter(
      (step) => !hidden_steps.includes(step.id) && step.name !== 6
    )

    return (
      <div
        style={{
          display: 'flex',
          flexWrap: 'wrap',
          gap: '20px',
          justifyContent: 'flex-start',
        }}
      >
        <React.Fragment>
          {visibleSteps.map((step) => (
            <StepElement
              key={step.id}
              step={step}
              archive={archive}
              onStepRetry={this.onStepRetry}
              onStepContinue={this.onStepContinue}
              canExecuteStep={canExecuteStep}
            />
          ))}
          {canExecuteStep && (
            <AddStepToPipeline
              archive={archive}
              nextSteps={nextSteps}
              onAddStep={this.onAddStep}
              onAddPipeline={this.onAddPipeline}
            />
          )}
        </React.Fragment>
      </div>
    )
  }
}
