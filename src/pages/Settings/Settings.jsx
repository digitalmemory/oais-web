import React from 'react'
import { api } from '@/api.js'
import {
  Table,
  Loader,
  Button,
  Form,
  Grid,
  Input,
  Modal,
  Icon,
} from 'semantic-ui-react'
import PropTypes from 'prop-types'
import { SourceStatusList } from '@/pages/Home/HomeSourceStatus.jsx'

import { sendNotification } from '@/utils.js'

export class Settings extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: true, // Shows a loading spinner till the settings are fetched from the API call
      settings: null, // Stores the settings
      userSettings: null,
    }
  }

  getSettings = () => api.settings()
  getUserSettings = () => api.getUserSettings()

  loadSettings = async () => {
    try {
      const userSettings = await this.getUserSettings()
      this.setState({ settings: {}, userSettings: userSettings })
    } catch (e) {
      sendNotification('Error while fetching settings', e.message, 'error')
    } finally {
      this.setState({ loading: false })
    }
  }

  componentDidMount() {
    this.loadSettings()
  }
  
  updateSettings() {
    this.setState({ loading: true })
    this.loadSettings()
  }

  render() {
    const { settings, userSettings, loading } = this.state

    const loadingSpinner = <Loader active inline="centered" />

    return (
      <React.Fragment>
        {loading || !userSettings ? (
          <div> {loadingSpinner} </div>
        ) : (
          <div>
            <h1>User Settings</h1>
            Here you can set various values and authentication tokens needed to
            allow the platform to access your private records. <br></br>Check
            the help on each line to learn how to get those values.
            <Table celled>
              <Table.Header>
                <Table.Row>
                  <Table.HeaderCell>API Key</Table.HeaderCell>
                  <Table.HeaderCell width={12}>Value</Table.HeaderCell>
                </Table.Row>
              </Table.Header>

              <Table.Body>
                {userSettings["api_key"].map((api_key) => (
                  <SettingItem
                    editable={true}
                    key={api_key["source_id"]}
                    sourceId={api_key["source_id"]}
                    title={api_key["source"]}
                    value={api_key["key"]}
                    updateSettings={this.updateSettings.bind(this)}
                    modalInfo={api_key["how_to"]}
                  />
              ))}
              </Table.Body>
            </Table>
          </div>
        )}
        <h1>Sources availability</h1>
        The following table shows the availability of the various sources.<br></br><br></br>
        <SourceStatusList />
      </React.Fragment>
    )
  }
}

class SettingItem extends React.Component {
  static propTypes = {
    editable: PropTypes.bool.isRequired,
    sourceId: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    value: PropTypes.node,
    updateSettings: PropTypes.func.isRequired,
    modalInfo: PropTypes.string,
  }

  constructor(props) {
    super(props)
    this.state = {
      editMode: false,
      showToken: false,
    }
  }

  toggleEditMode = async (e) => {
    e.preventDefault()
    this.setState({ editMode: !this.state.editMode })
  }

  seeToken = async (e) => {
    e.preventDefault()
    this.setState({ showToken: !this.state.showToken })
  }

  deleteToken = async (e) => {
    e.preventDefault()
    try {
      const newSettings = await api.setUserSettings(this.props.sourceId, null)
      this.props.updateSettings()
    } catch (e) {
      sendNotification('Error while deleting token', e.message, 'error')
    }
  }

  render() {
    const { title, sourceId, value, editable } = this.props

    let titleCell = title
    let valueCell = value
    const editButton = (
      <Button
        circular
        size="small"
        basic
        icon="pencil alternate"
        onClick={this.toggleEditMode}
      />
    )

    let token = this.props.value
    if (!this.state.showToken) {
      token = '******************'
    }

    const modal = this.props.modalInfo && this.props.modalInfo != "" ? <Modal
      key={titleCell}
      trigger={<Icon link name="help circle" />}
      header={"Obtaining " + titleCell + " API Key"}
      content={this.props.modalInfo}
      actions={['Back']}
    /> : null

    if (editable) {
      if (value == '' || value == null) {
        valueCell = (
          <Grid>
            <Grid.Column>
              <i>-</i>
            </Grid.Column>
            <Grid.Column floated="right" textAlign="right" width={3}>
              <Button.Group>{editButton}</Button.Group>
            </Grid.Column>
          </Grid>
        )
      } else {
        valueCell = (
          <Grid columns="equal">
            <Grid.Column>{token}</Grid.Column>
            <Grid.Column width={6} textAlign="right">
              <Button.Group>
                <Button
                  circular
                  size="small"
                  basic
                  icon="trash"
                  onClick={this.deleteToken}
                />
                <Button
                  circular
                  size="small"
                  basic
                  icon={this.state.showToken ? 'eye slash' : 'eye'}
                  onClick={this.seeToken}
                />
                {editButton}
              </Button.Group>
            </Grid.Column>
          </Grid>
        )
      }
    }

    return (
      <Table.Row>
        <Table.Cell>
          <Grid columns={"equal"}>
            <Grid.Column width={12}>{titleCell}</Grid.Column>
            <Grid.Column textAlign="right">{modal}</Grid.Column>
          </Grid>
        </Table.Cell>
        <Table.Cell>
          {this.state.editMode ? (
            <EditKeyForm
              size="small"
              title={title}
              sourceId={sourceId}
              value={value}
              toggleEdit={this.toggleEditMode.bind(this)}
              updateSettings={this.props.updateSettings}
            />
          ) : (
            valueCell
          )}
        </Table.Cell>
      </Table.Row>
    )
  }
}

class EditKeyForm extends React.Component {
  static propTypes = {
    title: PropTypes.string,
    sourceId: PropTypes.number,
    value: PropTypes.node,
    toggleEdit: PropTypes.func,
    updateSettings: PropTypes.func,
  }

  constructor(props) {
    super(props)
    this.state = {
      editValue: this.props.value,
    }
  }

  handleValueChange = (event) => {
    this.setState({ editValue: event.target.value })
  }

  handleSubmit = async (event) => {
    event.preventDefault()
    try {
      const newSettings = await api.setUserSettings(
        this.props.sourceId,
        this.state.editValue
      )
      if (this.props.value !== this.state.editValue) {
        sendNotification(this.props.title, ' API Key changed successfully!', 'success')
      }
      this.props.updateSettings()
    } catch (e) {
      sendNotification('Error while modifying settings', e.message, 'error')
    }
  }

  render() {
    const { title, value } = this.props
    return (
      <Form>
        <Grid stackable columns="equal">
          <Grid.Column>
            <Form.Field
              size="small"
              control={Input}
              value={this.state.editValue}
              onChange={this.handleValueChange}
              placeholder={'Enter ' + title + ' API Key here...'}
            />
          </Grid.Column>
          <Grid.Column textAlign="right" width={3}>
            <Button.Group>
              <Button
                circular
                basic
                icon="undo"
                size="small"
                onClick={this.props.toggleEdit}
              />
              <Button
                primary
                circular
                size="small"
                onClick={this.handleSubmit}
                icon="check"
              />
            </Button.Group>
          </Grid.Column>
        </Grid>
      </Form>
    )
  }
}
