import React from 'react'
import { Redirect } from 'react-router-dom'
import { api } from '@/api.js'
import { Button, Grid, Form, Input, Icon, Modal } from 'semantic-ui-react'
import { sendNotification } from '@/utils.js'

export class BatchAnnounce extends React.Component {
  /**
   * URL parse component contains a form field where the
   * user can enter a path where an SIP was already uploaded
   * and announce it to the system
   */
  state = {
    batchAnnouncePath: '', // Stores the url to be parsed
    batchTag: '', // Batches need a tag
    isRedirect: false, // Triggered when the user presses the submit button and redirects to the created archive page
    redirectToCollection: null,
    open: false,
    requestInProgress: false,
  }

  handleConfirmedSubmit = async () => {
    /**
     * Called when the user presses the submit button,
     * then passes the url to the backend and gets the id for the newly created tag.
     * If the api call is successful, calls the handleRedirect function.
     * If there is an error sends a notification to the user
     */
    if(this.state.open) {
      try {
        this.setState({requestInProgress : true})
        const response = await api.batchAnnounce(this.state.batchAnnouncePath, this.state.batchTag)
        this.setState({
          redirectToCollection: response.id,
          isRedirect: true,
        })
        sendNotification(
          'Success',
          'SIP folder sent successfully to OAIS platform',
          'success'
        )
      } catch (e) {
        sendNotification('Error while parsing URL', e.message, 'error')
      } finally {
        this.setState({open : false, requestInProgress : false})
      }
    }
  }

  handlePathChange = (event) => {
    /**
     * Updates the path state each time the url text in the form changes
     */
    event.preventDefault()
    this.setState({ batchAnnouncePath: event.target.value })
  }

  handleTagChange = (event) => {
    /**
     * Updates the tag state each time the tag name in the form changes
     */
    event.preventDefault()
    this.setState({ batchTag: event.target.value })
  }

  handleModalOpen = (open) => {
    if(open && this.state.batchAnnouncePath && this.state.batchTag) {
      this.setState({open : true})
    } else {
      this.setState({open : false})
    }
  }

  render() {
    const { isRedirect } = this.state

    return (
      <React.Fragment>
        <h1>Batch Announce Submission Bag Folders</h1>
        <p>
          If you already uploaded your SIPs on EOS, you can add it to the
          platform by entering the parent folder&apos;s absolute path here. Make sure you have
          granted the necessary permissions (give the &quot;oais&quot; user read
          access if the folder is private) and that the path directly points to
          the folder containing the SIP folders (i.e. it contains SIP folders).
        </p>
        <Form>
          <Grid columns={3} stackable>
            <Grid.Column width={9} verticalAlign="middle">
              <Form.Field
                control={Input}
                value={this.state.batchAnnouncePath}
                onChange={this.handlePathChange}
                label="EOS Path"
                placeholder="/eos/home-u/user/sip_parent_folder"
                required={true}
              />
            </Grid.Column>
            <Grid.Column width={4}>
              <Form.Field
                control={Input}
                value={this.state.batchTag}
                onChange={this.handleTagChange}
                label="Tag"
                placeholder="ilcdoc_2023"
                required={true}
              />
            </Grid.Column>
            <Grid.Column verticalAlign="bottom" width={3}>
              <Modal
                onClose={() => this.handleModalOpen(false)}
                onOpen={() => this.handleModalOpen(true)}
                open={this.state.open}
                trigger={<Button primary fluid onClick={() => this.handleModalOpen(true)}>
                          <Icon name="rss" />
                          Announce
                        </Button>}
              >
                <Modal.Header>Batch announce folder</Modal.Header>
                <Modal.Content image>
                  <Modal.Description>
                    Are you sure you want to Batch Announce the given folder: <b>{this.state.batchAnnouncePath}</b> ?
                    <br/>
                    <b>NOTE:</b> All containing subfolders will be announced to the platform as a separate SIP.
                  </Modal.Description>
                </Modal.Content>
                <Modal.Actions>
                  <Button onClick={() => this.handleModalOpen(false)} disabled={this.state.requestInProgress}>
                    Cancel
                  </Button>
                  {this.state.requestInProgress ? 
                    <Button primary disabled type="submit">
                      <Icon loading name="spinner" color="blue"/>
                      Announce
                    </Button>
                  :
                    <Button primary positive type="submit" onClick={() => {this.handleConfirmedSubmit()}}>
                      <Icon name="rss" />
                      Announce
                    </Button>
                }
                </Modal.Actions>
              </Modal>
            </Grid.Column>
          </Grid>
        </Form>

        <p>{this.state.response}</p>
        {isRedirect && <Redirect to={"/collection/" + this.state.redirectToCollection}/>}
      </React.Fragment>
    )
  }
}
