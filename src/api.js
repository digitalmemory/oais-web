import { getCookie } from '@/utils.js'
import axios from 'axios'
import { AppContext } from '@/AppContext.js'
import { sendNotification } from '@/utils.js'
import store from './store/index'

// Base endpoint where the API is served
export const API_URL = '/api/'

/**
 * Provides functions to consume the oais-platform API
 *
 */

// Set the CSRF token in the X-CSRFToken header
// Check https://django.readthedocs.io/en/stable/ref/csrf.html#ajax
// for more information on the implemented flow
function addCSRFToken(options) {
  const CSRFToken = getCookie('csrftoken')

  if (!CSRFToken) {
    return options
  }

  options.headers ??= {}
  options.headers['X-CSRFToken'] = CSRFToken
  options.mode = 'same-origin'
  return options
}

class API {
  constructor(config) {
    this.client = axios.create(config)

    //  Setup interceptors
    this.client.interceptors.response.use(
      function (response) {
        return response
      },
      function (error) {
        // Bypass error redirection
        if (error.config && error.config.bypass) return Promise.reject(error)

        store.dispatch({
          type: 'setErrorStatus',
          status: error.response.status,
        })
        return Promise.reject(error)
      }
    )
  }

  static contextType = AppContext.Context

  static async handleError(request) {
    try {
      return await request()
    } catch (e) {
      let detail = e.message
      const response = e.response?.data
      if (e.response) {
        if (e.message == 'Network Error') {
          sendNotification(
            'Cannot connect to the server. Check your network connection.',
            e.message,
            'error'
          )
        }
      }
      if (response?.detail) {
        detail = response.detail
        if (detail == "Authentication credentials were not provided.") {
          //CSRF token expired, redirect to login
          AppContext.logout()
        }
      } else if (response?.non_field_errors) {
        detail = response.non_field_errors.join('\n')
      }
      throw new Error(detail)
    }
  }

  async _get(url, options = {}) {
    options = addCSRFToken(options)
    const { data: response } = await API.handleError(
      async () => await this.client.get(url, options)
    )
    return response
  }

  async _post(url, data = {}, options = {}) {
    options = addCSRFToken(options)
    const { data: response } = await API.handleError(
      async () => await this.client.post(url, data, options)
    )
    return response
  }

  async _patch(url, data = {}, options = {}) {
    options = addCSRFToken(options)
    const { data: response } = await API.handleError(
      async () => await this.client.patch(url, data, options)
    )
    return response
  }

  async login(username, password) {
    return await this._post('/login/', {
      username: username,
      password: password,
    })
  }

  async logout() {
    return await this._post('/logout/')
  }

  async search(source, query, page, size) {
    return await this._get(`/search/${source}/`, {
      params: { q: query, p: page, s: size },
    })
  }

  async searchById(source, id) {
    return await this._get(`/search/${source}/${id}/`)
  }

  // async internal_search(searchQuery, searchAgg) {
  //   return await this._post(`/search-query/`, {
  //     query: { query_string: { query: searchQuery }, from: 0 },
  //     aggs: { terms: { field: searchAgg } },
  //   })
  // }

  async createStagedArchive(records) {
    return await this._post(`/users/me/stage/`, { records: records })
  }

  async parseURL(url) {
    return await this._post('/search/parse-url/', { url: url })
  }

  async archiveDetails(id) {
    return await this._get(`/archives/${id}/`)
  }

  async archiveRunPipeline(archive, run_type = 'run', steps = []) {
    return await this._post(`archives/${archive.id}/pipeline/`, {
      pipeline_steps: steps,
      archive: archive,
      run_type: run_type,
    })
  }

  async createUploadJob() {
    return this._post(`upload/jobs/create/`)
  }

  async addFileToUploadJob(uploadJobId, file) {
    let formData = new FormData()
    formData.append(file.webkitRelativePath, file)
    return await this._post(`upload/jobs/${uploadJobId}/add/file/`, formData)
  }

  async uploadFolder(files) {
    const { uploadJobId } = await this.createUploadJob()

    for (const file of files) {
      let formData = new FormData()
      formData.append('file', file)
      await this.addFileToUploadJob(uploadJobId, file)
    }

    await this._post(`upload/jobs/${uploadJobId}/sip/`)

    return await this._post(`upload/jobs/${uploadJobId}/archive/`)
  }

  async upload(file) {
    var formData = new FormData()
    formData.append('file', file)
    return await this._post(`/upload/sip`, formData)
  }

  // TODO: unify all uploading logic
  // async upload(files, compressed_sip = False) {
  //   const { uploadJobId } = await this.createUploadJob()
  //   for (const file of files) await this.addFileToUploadJob(uploadJobId, file)
  //
  //   if (compressed_sip) {
  //
  //   } {
  //     await this._post(`upload/jobs/`)
  //   }
  //
  // }

  async archives(page = 1, size = 20, access = 'all') {
    let params = {
      page,
      size,
      access,
    }

    let notNullParams = {}
    Object.keys(params).forEach((item) => {
      if (params[item]) {
        notNullParams[item] = params[item]
      }
    })
    return await this._get('/archives/', { params: notNullParams })
  }

  async filtered_archives(
    page = 1,
    size = 20,
    access = 'all',
    state,
    source,
    tag,
    step_name,
    step_status,
    query
  ) {
    let filters = {
      state,
      source,
      tag,
      step_name,
      step_status,
      query,
    }

    let notNullFilters = {}
    Object.keys(filters).forEach((item) => {
      if (filters[item]) {
        notNullFilters[item] = filters[item]
      }
    })
    return await this._post(
      '/archives/filter/',
      { filters: notNullFilters },
      { params: { page, size, access } }
    )
  }

  async stagedArchives() {
    return await this._get('/users/me/staging-area/?paginated=false')
  }

  async stagedArchivesPaginated(page) {
    return await this._get('/users/me/staging-area', { params: { page } })
  }

  async archivesByUser(id, page = 1) {
    return await this._get(`/users/${id}/archives/`, { params: { page } })
  }

  async archivesByUserStaged(id, page = 1) {
    return await this._get(`/users/${id}/archives-staged/`, {
      params: { page },
    })
  }

  async getArchivesDetailed(archives) {
    return await this._post(`/archives/details/`, { archives: archives })
  }

  async getArchiveSteps(id, bypass = false) {
    return await this._get(`/archives/${id}/steps/`, { bypass })
  }

  async getArchiveNextSteps(id) {
    return await this._get(`/archives/${id}/next-steps/`)
  }

  async getStep(id) {
    return await this._get(`/steps/${id}/`)
  }

  async user(id) {
    return await this._get(`/users/${id}/`)
  }

  async settings() {
    return await this._get('/settings/')
  }

  async getStepOrderConstraints() {
    return await this._get('/steps/constraints/')
  }

  async approveArchive(id) {
    return await this._post(`/steps/${id}/approve/`)
  }

  async rejectArchive(id) {
    return await this._post(`/steps/${id}/reject/`)
  }

  async collections(page = 1, internal) {
    return await this._get('/tags/', { params: { page, internal } })
  }

  async saveManifest(id, manifest) {
    return await this._post(`/archives/${id}/save-manifest/`, {
      manifest: manifest,
    })
  }

  async getAllTags() {
    return await this._get('/users/me/tags')
  }

  async getSourceStatus() {
    return await this._get('/users/me/sources')
  }

  async unstageArchive(id) {
    return await this._get(`/archives/${id}/unstage`)
  }

  async unstageArchives(archives) {
    return await this._post(`/archives/unstage/`, { archives: archives })
  }

  async deleteStagedArchive(id) {
    return await this._post(`/archives/${id}/delete-staged/`)
  }

  async getArchivesActions(archives) {
    return await this._post(`/archives/actions/`, { archives: archives })
  }

  async getArchivesDuplicates(records) {
    return await this._post(`/archives/duplicates/`, { records: records })
  }

  async collection(id, internal) {
    return await this._get(`/tags/${id}`, { params: { internal } })
  }

  async addArchivesToCollection(id, archives) {
    return await this._post(`/tags/${id}/add/`, {
      archives: archives,
    })
  }

  async removeArchivesFromCollection(id, archives) {
    return await this._post(`/tags/${id}/remove/`, {
      archives: archives,
    })
  }

  async collectionDelete(id) {
    return await this._post(`/tags/${id}/delete/`)
  }

  async collectionCreate(title, description, archives) {
    return await this._post(`/tags/create/`, {
      title: title,
      description: description,
      archives: archives,
    })
  }

  async getStepsStatus(status, name) {
    return await this._get(`/users/me/stats`, {
      status: status,
      name: name,
    })
  }

  async getArchiveCollections(id, bypass = false) {
    return await this._get(`/archives/${id}/tags/`, { bypass })
  }

  // async getArchiveExists(id) {
  //   return await this._get(`/archives/${id}/search/`)
  // }

  //API call to set api keys (works with the new api refactor)
  async setUserSettings(sourceId, token) {
    return await this._post(`/users/me/`, {
      source: sourceId,
      key: token,
    })
  }

  async getUserSettings() {
    return await this._get('/users/me/')
  }

  async announce(path) {
    return await this._post(`/upload/announce/`, {
      announce_path: path,
    })
  }

  async batchAnnounce(path, tag) {
    return await this._post(`/upload/batch-announce/`, {
      batch_announce_path: path,
      batch_tag: tag,
    })
  }

  async getAllTagNames() {
    return await this._get('/tags/names')
  }

  async getSources() {
    return await this._get('/sources')
  }

  async getArchivesSources() {
    return await this._get('/archives/sources')
  }
}

export const api = new API(
  {
    baseURL: API_URL,
  },
  store
)
