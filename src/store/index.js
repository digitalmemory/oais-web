import { createStore, applyMiddleware } from 'redux'
import reduxThunk from 'redux-thunk'

const initialState = {
  query: '',
  source: null,
  searchById: false,
  checkedRecords: [],
  status: '',
}

// Redux state stores three values: query, source and searchByID
const searchReducer = (state = initialState, action) => {
  // Action to change the value of the query
  if (action.type === 'setQuery') {
    return {
      query: action.query,
      source: state.source,
      searchById: state.searchById,
      checkedRecords: state.checkedRecords,
      status: state.status,
    }
  }
  // Action to change the value of the source
  if (action.type === 'setSource') {
    return {
      query: state.query,
      source: action.source,
      searchById: state.searchById,
      checkedRecords: state.checkedRecords,
      status: state.status,
    }
  }
  // Action to toogle the ID search
  if (action.type === 'setID') {
    return {
      query: state.query,
      source: state.source,
      searchById: !state.searchById,
      checkedRecords: state.checkedRecords,
      status: state.status,
    }
  }

  if (action.type === 'addRecord') {
    /*
      Checks if a record is in the checkedList and if not it appends it
      */
    let new_list = state.checkedRecords.concat(action.record)
    if (state.checkedRecords.length != 0) {
      state.checkedRecords.map((checkedRecord) => {
        if (checkedRecord == action.record) {
          new_list = state.checkedRecords
        }
      })
    }
    return {
      query: state.query,
      source: state.source,
      searchById: state.searchById,
      checkedRecords: new_list,
      status: state.status,
    }
  }

  if (action.type === 'removeRecord') {
    /*

      */
    let new_list = []
    state.checkedRecords.map((uncheckedRecord) => {
      if (uncheckedRecord == action.record) {
        new_list = state.checkedRecords.filter((item) => item != action.record)
      }
    })
    return {
      query: state.query,
      source: state.source,
      searchById: state.searchById,
      checkedRecords: new_list,
      status: state.status,
    }
  }

  if (action.type === 'removeAll') {
    /*
        Removes all records from the checklist
      */
    return {
      query: state.query,
      source: state.source,
      searchById: state.searchById,
      checkedRecords: [],
      status: state.status,
    }
  }

  if (action.type === 'addAll') {
    /* 
      For all the records, adds them in the list 
    */
    const new_list = action.records
    return {
      query: state.query,
      source: state.source,
      searchById: state.searchById,
      checkedRecords: new_list,
      status: state.status,
    }
  }

  if (action.type === 'setErrorStatus') {
    /* 
      Sets error status 
    */
    const err = action.status
    return {
      query: state.query,
      source: state.source,
      searchById: state.searchById,
      checkedRecords: state.checkedRecords,
      status: err,
    }
  }

  return state
}

const createStoreWithMiddleware = applyMiddleware(reduxThunk)(createStore)
const store = createStoreWithMiddleware(searchReducer)

export default store
